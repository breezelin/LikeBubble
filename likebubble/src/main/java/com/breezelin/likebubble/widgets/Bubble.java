package com.breezelin.likebubble.widgets;

/*
 * Created by Breeze Lin
 * 2016/7/12 15:21
 * breezesummerlin@163.com
 */

import android.graphics.Bitmap;

/**
 * 泡泡实体
 */
public class Bubble {

	/**
	 * 图片
	 */
	final Bitmap bubbleBitmap;
	/**
	 * 用于计算路线的常数
	 */
	float range;
	/**
	 * 已经飞行的帧数
	 */
	int t;
	/**
	 * 坐标x
	 */
	int x;
	/**
	 * 坐标y
	 */
	int y;
	/**
	 * 透明度
	 */
	int alpha;

	/**
	 * 点赞泡泡
	 *
	 * @param bubbleBitmap 泡泡的图片
	 */
	public Bubble(Bitmap bubbleBitmap) {
		this.bubbleBitmap = bubbleBitmap;
//		bubbleMatrix = new Matrix();
		reset();
	}

	/**
	 * 泡泡属性重置
	 */
	public void reset() {
		range = 0;
		t = 0;
		x = 0;
		y = 0;
		alpha = 255;
		// 重置绘制矩阵属性
//		bubbleMatrix.setTranslate(0, 0);
//		bubbleMatrix.setScale(1, 1);
//		bubbleMatrix.setRotate(0);
	}
}
